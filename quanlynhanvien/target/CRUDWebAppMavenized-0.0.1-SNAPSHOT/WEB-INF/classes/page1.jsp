<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>JavaSpring</title>
    <meta http-equiv ="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body>
<center><h1>Quan ly phong ban</h1></center>
<br>
<%--@elvariable id="phongban" type=""--%>
<form:form action="phongban.do" method="POST" commandName="phongban">
    <table>
        <tr>
            <td>Ma PB</td>
            <td><form:input path="maPB" /></td>
        </tr>
        <tr>
            <td>Ten PB</td>
            <td><form:input path="tenPB" /></td>
        </tr>
        <tr>
            <td>Dia Chi</td>
            <td><form:input path="diachi" /></td>
        </tr>
        <tr>
            <td>SDT</td>
            <td><form:input path="sdt" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" name="action" value="Add" />
                <input type="submit" name="action" value="Edit" />
                <input type="submit" name="action" value="Delete" />
                <input type="submit" name="action" value="Search" />
            </td>
        </tr>
    </table>
</form:form>
<br>
<table border="1">
    <tr>
        <td>Ma PB</td><td>Ten PB</td><td>Dia Chi</td><td>SDT</td>
    </tr>
    <c:forEach items="${dsphongban}" var="phongban">
        <tr>
            <td>${phongban.maPB}</td>
            <td>${phongban.tenPB}</td>
            <td>${phongban.diachi}</td>
            <td>${phongban.sdt}</td>
        </tr>
    </c:forEach>
</table>
<center><h1>Quan ly nhan vien</h1></center>
<br>
<%--@elvariable id="nhanvien" type=""--%>
<form:form action="phongban.do" method="POST" commandName="nhanvien">
    <table>
        <tr>
            <td>Ma NV</td>
            <td><form:input path="manv" /></td>
        </tr>
        <tr>
            <td>Ten NV</td>
            <td><form:input path="hoTen" /></td>
        </tr>
        <tr>
            <td>Gioi Tinh</td>
            <td><form:input path="gioiTinh" /></td>
        </tr>
        <tr>
            <td>Que Quan</td>
            <td><form:input path="queQuan" /></td>
        </tr>        <tr>
            <td>Ngay Sinh</td>
            <td><form:input path="ngaySinh" /></td>
        </tr>        <tr>
            <td>Dan Toc</td>
            <td><form:input path="danToc" /></td>
        </tr>        <tr>
            <td>SDT</td>
            <td><form:input path="sdt" /></td>
        </tr>        <tr>
            <td>Ten PB</td>
            <td><form:input path="tenPB" /></td>
        </tr>        <tr>
            <td>Chuc Vu</td>
            <td><form:input path="chucVu" /></td>
        </tr>        <tr>
            <td>Luong</td>
            <td><form:input path="luong" /></td>
        </tr>        <tr>
            <td>Trinh Do HV</td>
            <td><form:input path="trinhDoHV" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" name="action" value="AddNV" />
                <input type="submit" name="action" value="EditNV" />
                <input type="submit" name="action" value="DeleteNV" />
                <input type="submit" name="action" value="SearchNV" />
            </td>
        </tr>
    </table>
</form:form>
<br>
<table border="1">
    <tr>
        <td>Ma NV</td><td>Ten NV</td><td>Gioi Tinh</td><td>Que Quan</td><td>Ngay Sinh</td>
        <td>Dan Toc</td><td>SDT</td><td>Ten PB</td><td>Chuc Vu</td><td>Luong</td><td>Trinh Do HV</td>
    </tr>
    <c:forEach items="${dsnhanvien}" var="nhanvien">
        <tr>
            <td>${nhanvien.manv}</td>
            <td>${nhanvien.hoTen}</td>
            <td>${nhanvien.gioiTinh}</td>
            <td>${nhanvien.queQuan}</td>
            <td>${nhanvien.ngaySinh}</td>
            <td>${nhanvien.danToc}</td>
            <td>${nhanvien.sdt}</td>
            <td>${nhanvien.tenPB}</td>
            <td>${nhanvien.chucVu}</td>
            <td>${nhanvien.luong}</td>
            <td>${nhanvien.trinhDoHV}</td>
        </tr>
    </c:forEach>
</table>
</body>

</html>