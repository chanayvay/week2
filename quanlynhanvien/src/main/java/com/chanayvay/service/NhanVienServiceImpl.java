package com.chanayvay.service;

import com.chanayvay.dao.QuanLy;
import com.chanayvay.model.NhanVien;
import java.util.List;
import org.springframework.context.annotation.Configuration;

@Configuration
public class NhanVienServiceImpl implements NhanVienService {
  private QuanLy quanly;
  public void setQuanly(QuanLy quanly){
    this.quanly = quanly;
  }

  public void add(NhanVien nhanVien) {
    this.quanly.add(nhanVien);
  }

  public void update(NhanVien nhanVien) {
    this.quanly.update(nhanVien);
  }

  public void del(int id) {
  this.quanly.del(id);
  }

  public NhanVien find(int id) {
    return this.quanly.find(id);
  }

  public List<NhanVien> list() {
    return this.quanly.list();
  }
}
