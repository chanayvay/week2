package com.chanayvay.service;

import com.chanayvay.model.NhanVien;
import java.util.List;

public interface NhanVienService {
  public void add(NhanVien nhanVien);
  public void update(NhanVien nhanVien);
  public void del(int id);
  public NhanVien find(int id);
  public List<NhanVien> list();
}
