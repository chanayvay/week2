package com.chanayvay.service;
import com.chanayvay.dao.QuanLyNhanVien;
import com.chanayvay.dao.QuanLyPB;
import com.chanayvay.dao.QuanLyPhongBan;
import com.chanayvay.model.PhongBan;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PhongBanServiceImpl implements PhongBanService{

  @Autowired
  QuanLyPhongBan quanLyPB;
//  public void setQuanLyPB(QuanLyPB quanLyPB){
//    quanLyPB = quanLyPB;
//  }

  public void add(PhongBan phongBan) {
    quanLyPB.add(phongBan);
  }

  public void update(PhongBan phongBan) {
    quanLyPB.update(phongBan);
  }

  public void del(int id) {
    quanLyPB.del(id);
  }

  public PhongBan find(int id) {
    return quanLyPB.find(id);
  }

  public List<PhongBan> list() {
    return quanLyPB.list();
  }
}
