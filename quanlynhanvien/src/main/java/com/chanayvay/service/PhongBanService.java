package com.chanayvay.service;

import com.chanayvay.model.PhongBan;
import java.util.List;

public interface PhongBanService {
  public void add(PhongBan phongBan);
  public void update(PhongBan phongBan);
  public void del(int id);
  public PhongBan find(int id);
  public List<PhongBan> list();
}
