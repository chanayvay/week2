package com.chanayvay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class NhanVien {
  @Id
  int manv;
  @Column
  String hoTen;
  @Column
  String gioiTinh;
  @Column
  String queQuan;
  @Column
  String ngaySinh;
  @Column
  String danToc;
  @Column
  String sdt;
  @Column
  String tenPB;
  @Column
  String chucVu;
  @Column
  int luong;
  @Column
  String trinhDoHV;

  public NhanVien(){

  }
  public NhanVien(int manv,String hoTen,String gioiTinh,String queQuan,String ngaySinh,
      String danToc,String sdt,String tenPB,String chucVu,int luong,String trinhDoHV){
    this.manv = manv;
    this.hoTen = hoTen;
    this.gioiTinh = gioiTinh;
    this.queQuan = queQuan;
    this.ngaySinh  = ngaySinh;
    this.danToc = danToc;
    this.sdt = sdt;
    this.tenPB = tenPB;
    this.chucVu = chucVu;
    this.luong = luong;
    this.trinhDoHV = trinhDoHV;
  }

  public int getManv() {
    return manv;
  }

  public String getDanToc() {
    return danToc;
  }

  public String getGioiTinh() {
    return gioiTinh;
  }

  public String getHoTen() {
    return hoTen;
  }

  public String getNgaySinh() {
    return ngaySinh;
  }

  public String getQueQuan() {
    return queQuan;
  }

  public String getSdt() {
    return sdt;
  }

  public String getTenPB() {
    return tenPB;
  }

  public int getLuong() {
    return luong;
  }

  public String getChucVu() {
    return chucVu;
  }

  public String getTrinhDoHV() {
    return trinhDoHV;
  }

  public void setTenPB(String tenPB) {
    this.tenPB = tenPB;
  }

  public void setManv(int manv) {
    this.manv = manv;
  }

  public void setSdt(String sdt) {
    this.sdt = sdt;
  }

  public void setChucVu(String chucVu) {
    this.chucVu = chucVu;
  }

  public void setDanToc(String danToc) {
    this.danToc = danToc;
  }

  public void setGioiTinh(String gioiTinh) {
    this.gioiTinh = gioiTinh;
  }

  public void setHoTen(String hoTen) {
    this.hoTen = hoTen;
  }

  public void setLuong(int luong) {
    this.luong = luong;
  }

  public void setNgaySinh(String ngaySinh) {
    this.ngaySinh = ngaySinh;
  }

  public void setQueQuan(String queQuan) {
    this.queQuan = queQuan;
  }

  public void setTrinhDoHV(String trinhDoHV) {
    this.trinhDoHV = trinhDoHV;
  }
}
