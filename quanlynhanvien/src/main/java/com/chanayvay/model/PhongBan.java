package com.chanayvay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class PhongBan {
  @Id
  private int maPB;
  @Column
  private String tenPB;
  @Column
  private String diachi;
  @Column
  private String sdt;
  public PhongBan(){

  }
  public PhongBan(int maPhongBan,String tenPhongBan,String diachi,String sdt){
    this.tenPB = tenPhongBan;
    this.diachi = diachi;
    this.sdt = sdt;
    this.maPB = maPhongBan;
  }

  public void setDiachi(String diachi) {
    this.diachi = diachi;
  }

  public void setMaPB(int maPB) {
    this.maPB = maPB;
  }

  public void setSdt(String sdt) {
    this.sdt = sdt;
  }

  public void setTenPB(String tenPB) {
    this.tenPB = tenPB;
  }

  public int getMaPB() {
    return maPB;
  }

  public String getSdt() {
    return sdt;
  }

  public String getDiachi() {
    return diachi;
  }

  public String getTenPB() {
    return tenPB;
  }
}
