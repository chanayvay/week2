package com.chanayvay.controller;

import com.chanayvay.dao.QuanLyNhanVien;
import com.chanayvay.dao.QuanLyPhongBan;
import com.chanayvay.model.NhanVien;
import com.chanayvay.model.PhongBan;
import com.chanayvay.service.PhongBanServiceImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
//@RequestMapping("/home")
public class Home {

  @Autowired
  private PhongBanServiceImpl phongBanService;

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public String home(ModelMap map) {
    QuanLyPhongBan quanLyPhongBan = new QuanLyPhongBan();
    PhongBan phongBan = new PhongBan();
    QuanLyNhanVien quanLyNhanVien = new QuanLyNhanVien();
    NhanVien nhanVien = new NhanVien();
    map.put("phongban",phongBan);
    map.put("nhanvien",nhanVien);
    map.put("dsphongban",quanLyPhongBan.list());
    map.put("dsnhanvien",quanLyNhanVien.list());
    return "/page1.jsp";
  }
  @RequestMapping(value="/phongban.do", method=RequestMethod.POST)
  public String doActions(@ModelAttribute PhongBan student,NhanVien nhanVien, BindingResult result
      , @RequestParam String action, Map<String, Object> map){
    QuanLyPhongBan quanLyPhongBan = new QuanLyPhongBan();
    PhongBan studentResult = new PhongBan();
    switch(action.toLowerCase()){	//only in Java7 you can put String in switch
      case "add":
        quanLyPhongBan.add(student);
        studentResult = student;
        break;
      case "edit":
        quanLyPhongBan.update(student);
        studentResult = student;
        break;
      case "delete":
        quanLyPhongBan.del(student.getMaPB());
        studentResult = new PhongBan();
        break;
      case "search":
        PhongBan searchedStudent = quanLyPhongBan.find(student.getMaPB());
        studentResult = searchedStudent!=null ? searchedStudent : new PhongBan();
        break;
    }
    map.put("phongban", studentResult);
    map.put("dsphongban", quanLyPhongBan.list());
    QuanLyNhanVien quanLyNhanVien = new QuanLyNhanVien();
    NhanVien studentRs = new NhanVien();
    switch(action.toLowerCase()){	//only in Java7 you can put String in switch
      case "addnv":
        quanLyNhanVien.add(nhanVien);
        studentRs = nhanVien;
        break;
      case "editnv":
        quanLyNhanVien.update(nhanVien);
        studentRs = nhanVien;
        break;
      case "deletenv":
        quanLyNhanVien.del(nhanVien.getManv());
        studentRs = new NhanVien();
        break;
      case "searchnv":
        NhanVien searchedStudent1 = quanLyNhanVien.find(nhanVien.getManv());
        studentRs = searchedStudent1!=null ? searchedStudent1 : new NhanVien();
        break;
    }
    map.put("nhanvien", studentRs);
    map.put("dsnhanvien", quanLyNhanVien.list());
    return "/page1.jpg";
  }

  @RequestMapping("/page1")
  public String page1() {

    return "page1.jsp";
  }

}
