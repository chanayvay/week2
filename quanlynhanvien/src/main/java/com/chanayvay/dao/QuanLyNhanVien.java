package com.chanayvay.dao;

import com.chanayvay.model.NhanVien;
import com.chanayvay.model.PhongBan;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuanLyNhanVien implements QuanLy{

  public static SessionFactory sessionFactory;

  public void add(NhanVien nv) {
    sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    Transaction transaction = null;
    if (!checkexist(nv.getManv())) {
      System.out.println("Mã nhân viên đã tồn tại ....");
    } else if (!checkNameOfPB(nv.getTenPB())){
      System.out.println("Phòng ban không tồn tại");
    }
    else {
      try {
        transaction = session.beginTransaction();
        session.save(nv);
        transaction.commit();
      } catch (HibernateException e) {
        if (transaction != null) {
          transaction.rollback();
          e.printStackTrace();
        }
      } finally {
        session.close();
      }
    }
  }

  public void update(NhanVien nv) {
    sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    Transaction transaction = null;
    if (!checkexist(nv.getManv())&& checkNameOfPB(nv.getTenPB())) {
      try {
        transaction = session.beginTransaction();
        NhanVien nhanVien = (NhanVien) session.load(NhanVien.class, (Integer) nv.getManv());
        nhanVien.setHoTen(nv.getHoTen());
        nhanVien.setGioiTinh(nv.getGioiTinh());
        nhanVien.setQueQuan(nv.getQueQuan());
        nhanVien.setNgaySinh(nv.getNgaySinh());
        nhanVien.setDanToc(nv.getDanToc());
        nhanVien.setSdt(nv.getSdt());
        nhanVien.setTenPB(nv.getTenPB());
        nhanVien.setChucVu(nv.getChucVu());
        nhanVien.setLuong(nv.getLuong());
        nhanVien.setTrinhDoHV(nv.getTrinhDoHV());
        session.update(nhanVien);
        transaction.commit();
      } catch (HibernateException e) {
        if (transaction != null) {
          transaction.rollback();
          e.printStackTrace();
        }
      } finally {
        session.close();
      }
    }else if (!checkNameOfPB(nv.getTenPB())){
      System.out.println("Phòng ban không tồn tại ...");
    }
    else {
      System.out.println("Nhân viên không tồn tại ...");
    }
  }

  public void del(int manv) {
    sessionFactory = HibernateUtil.getSessionFactory();
    if (!checkexist(manv)) {
      Session session = sessionFactory.openSession();
      Transaction transaction = null;
      try {
        transaction = session.beginTransaction();
        NhanVien nhanVien = (NhanVien) session.get(NhanVien.class, (Integer) manv);
        session.delete(nhanVien);
        transaction.commit();
      } catch (HibernateException e) {
        if (transaction != null) {
          transaction.rollback();
          e.printStackTrace();
        }
      } finally {
        session.close();
      }
    } else {
      System.out.println("Nhân viên không tồn tại...");
    }

  }

  public NhanVien find(int manv) {
    sessionFactory = HibernateUtil.getSessionFactory();
    NhanVien nhanVien=null;
    if (!checkexist(manv)) {
      Session session = sessionFactory.openSession();
      Transaction transaction = null;
      try {
        transaction = session.beginTransaction();
        Query query = session.createQuery("from NhanVien");
        nhanVien = (NhanVien) session.load(NhanVien.class,new Integer(manv));
        List<NhanVien> nhanViens = (List<NhanVien>) query.list();
        for (NhanVien e : nhanViens) {
          if (e.getManv() == manv) {
            System.out.println("Mã nhân viên      : " + e.getManv());
            System.out.println("Họ tên            : " + e.getHoTen());
            System.out.println("Giới tính         : " + e.getGioiTinh());
            System.out.println("Quê quán          : " + e.getQueQuan());
            System.out.println("Ngày sinh         : " + e.getNgaySinh());
            System.out.println("Dân tộc           : " + e.getDanToc());
            System.out.println("SDT               : " + e.getSdt());
            System.out.println("Phòng ban         : " + e.getTenPB());
            System.out.println("Chức vụ           : " + e.getChucVu());
            System.out.println("Lương             : " + e.getLuong());
            System.out.println("Trình độ học vấn  : " + e.getTenPB());
          }
        }
      } catch (HibernateException e) {
        if (transaction != null) {
          transaction.rollback();
          e.printStackTrace();
        }
      } finally {
        session.close();
      }
    } else {
      System.out.println("Phòng ban không tồn tại ...");
    }
    return nhanVien;
  }

  public List<NhanVien> list() {
    sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    Transaction transaction = null;
    List<NhanVien> nhanViens = null;
      transaction = session.beginTransaction();
      Query query = session.createQuery("from NhanVien");
       nhanViens = (List<NhanVien>) query.list();
    return nhanViens;
  }

  public boolean checkexist(int manv) {
    sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    Transaction transaction = null;
    try {
      transaction = session.beginTransaction();
      Query query = session.createQuery("from NhanVien");
      List<NhanVien> nhanViens = (List<NhanVien>) query.list();
      for (NhanVien nv : nhanViens) {
        if (nv.getManv() == manv) {
          return false;
        }
      }
    } catch (HibernateException e) {
      if (transaction != null) {
        transaction.rollback();
        e.printStackTrace();
      }
    }
    return true;
  }
  public boolean checkNameOfPB(String tenpb){
    sessionFactory = HibernateUtil.getSessionFactory();
    boolean check = false;
    Session session = sessionFactory.openSession();
    Transaction transaction = null;
    try {
      transaction  = session.beginTransaction();
      Query  query = session.createQuery("from PhongBan");
      List<PhongBan> phongBans = (List<PhongBan>) query.list();
      for (PhongBan e:phongBans){
        if (tenpb.equals(e.getTenPB())){
          check = true;
        }
      }
    } catch (HibernateException e){
      if (transaction !=null){
        transaction.rollback();
        e.printStackTrace();
      }
    } finally {
      session.close();
    }
    return check;
  }

  public void quanLy() {
    System.out.println("Các chức năng:");
    System.out.println("1: Thêm nhân viên");
    System.out.println("2: Xóa nhân viên");
    System.out.println("3: Sửa nhân viên");
    System.out.println("4: Tìm nhân viên");
    System.out.println("5: In ra danh sách nhân viên");
    System.out.println("0: Thoát");
    boolean check = true;
    while (check) {
      System.out.println();
      System.out.print("Nhập vào lựa chọn: ");
      int k = Nhap.nhapSoNguyen();
      switch (k) {
        case 1:
          System.out.print("Mã nhân viên: ");
          int manv = Nhap.nhapSoNguyen();
          System.out.print("Họ tên: ");
          String hoTen = Nhap.nhapString();
          System.out.print("Giới tính: ");
          String gioiTinh = Nhap.nhapString();
          System.out.print("Quê quán: ");
          String queQuan = Nhap.nhapString();
          System.out.print("Ngày sinh: ");
          String ngaySinh = Nhap.nhapNS();
          System.out.print("Dân tộc: ");
          String danToc = Nhap.nhapString();
          System.out.print("Số điện thoại: ");
          String sdt = Nhap.nhapString();
          System.out.print("Tên Phòng ban: ");
          String tenPB = Nhap.nhapString();
          System.out.print("Chức vụ: ");
          String chucVu = Nhap.nhapString();
          System.out.print("Lương: ");
          int luong = Nhap.nhapSoNguyen();
          System.out.print("Trình độ học vấn: ");
          String trinhDoHV = Nhap.nhapString();
          NhanVien nhanvien = new NhanVien(manv, hoTen, gioiTinh, queQuan, ngaySinh
              , danToc, sdt, tenPB, chucVu, luong, trinhDoHV);
          add(nhanvien);
          System.out.print("--------------------------------");
          break;
        case 2:
          System.out.print("Nhập mã nhân viên cần xóa: ");
          manv = Nhap.nhapSoNguyen();
          del(manv);
          System.out.println("-------------------");
          break;
        case 3:
          System.out.print("Nhập mã nv cần thay thế:");
          int manvfix = Nhap.nhapSoNguyen();
          System.out.print("Họ tên: ");
          hoTen = Nhap.nhapString();
          System.out.print("Giới tính: ");
          gioiTinh = Nhap.nhapString();
          System.out.print("Quê quán: ");
          queQuan = Nhap.nhapString();
          System.out.print("Ngày sinh: ");
          ngaySinh = Nhap.nhapNS();
          System.out.print("Dân tộc: ");
          danToc = Nhap.nhapString();
          System.out.print("Số điện thoại: ");
          sdt = Nhap.nhapString();
          System.out.print("Tên Phòng ban: ");
          tenPB = Nhap.nhapString();
          System.out.print("Chức vụ: ");
          chucVu = Nhap.nhapString();
          System.out.print("Lương: ");
          luong = Nhap.nhapSoNguyen();
          System.out.print("Trình độ học vấn: ");
          trinhDoHV = Nhap.nhapString();
          nhanvien = new NhanVien(manvfix, hoTen, gioiTinh, queQuan, ngaySinh
              , danToc, sdt, tenPB, chucVu, luong, trinhDoHV);
          update(nhanvien);
          System.out.print("-------------------");
          break;
        case 4:
          System.out.println("Nhập mã nhân viên cần tìm: ");
          manv = Nhap.nhapSoNguyen();
          find(manv);
          System.out.print("-------------------");
          break;
        case 5:
          list();
          System.out.print("-----------------------------------------------------");
          break;
        case 0:
          check = false;
          break;
        default:
          System.out.println("Chưa có chức năng ...");
          break;
      }
    }
  }
}
