package com.chanayvay.dao;

import com.chanayvay.model.PhongBan;
import java.util.List;

public interface QuanLyPB {
  public void add(PhongBan phongBan);
  public void update(PhongBan phongBan);
  public void del(int id);
  public PhongBan find(int id);
  public List<PhongBan> list();
}
