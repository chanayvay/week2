package com.chanayvay.dao;

import java.util.Scanner;

public class Nhap {
    public static String nhapString(){
      String n="";
      Scanner nhap = new Scanner(System.in);
      n=nhap.nextLine();
      return n;
    }
    public static int nhapSoNguyen(){
      int n = 0;
      Scanner nhap = new Scanner(System.in);
      boolean check = true;
      while (check){
        try {
          n = nhap.nextInt();
          check = false;
        } catch (Exception e){
          System.out.println("Nhập số nguyên ....");
          nhap.nextLine();
        }
      }
      return n;
    }
    public static String nhapNS(){
      String n = "";
      Scanner nhap = new Scanner(System.in);
      boolean check = true;
      String cq = "[0-3]\\d\\-[jfmasond][aepuco][nbrylgptvc]\\-[12]\\d\\d\\d";
      while (check){
        n = nhap.nextLine();
        if (n.matches(cq)){
          check = false;
        }
        else{
          System.out.println("Nhập ngày-tháng-năm (xx-xxx-xxxx)");
        }
      }
        return n;
    }
}