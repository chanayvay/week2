package com.chanayvay.dao;

import com.chanayvay.model.NhanVien;
import com.chanayvay.model.PhongBan;
import java.util.List;

public interface QuanLy {
  public void add(NhanVien nhanVien);
  public void update(NhanVien nhanVien);
  public void del(int id);
  public NhanVien find(int id);
  public List<NhanVien> list();
}
