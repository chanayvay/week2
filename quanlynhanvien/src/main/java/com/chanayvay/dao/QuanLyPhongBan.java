package com.chanayvay.dao;

import com.chanayvay.model.NhanVien;
import com.chanayvay.model.PhongBan;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.annotation.Configuration;

@Configuration
public class QuanLyPhongBan implements QuanLyPB{

  public static SessionFactory sessionFactory;

  public void add(PhongBan pb) {
    sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    Transaction transaction = null;
    if (!checkexist(pb.getMaPB())) {
      System.out.println("Phòng ban đã tồn tại ....");
    } else {
      try {
        transaction = session.beginTransaction();
        session.save(pb);
        transaction.commit();
      } catch (HibernateException e) {
        if (transaction != null) {
          transaction.rollback();
          e.printStackTrace();
        }
      } finally {
        session.close();
      }
    }
  }

  public void update(PhongBan pb) {
    sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    Transaction transaction = null;
    if (!checkexist(pb.getMaPB())){
    try {
      transaction = session.beginTransaction();
      PhongBan phongBan = (PhongBan) session.load(PhongBan.class, (Integer) pb.getMaPB());
      phongBan.setTenPB(pb.getTenPB());
      phongBan.setDiachi(pb.getDiachi());
      phongBan.setSdt(pb.getSdt());
      session.update(phongBan);
      transaction.commit();

    } catch (HibernateException e) {
      if (transaction != null) {
        transaction.rollback();
        e.printStackTrace();
      }
    } finally {
      session.close();
    }
    } else {
      System.out.println("Phòng ban không tồn tại ...");
    }
  }

  public void del(int mapb) {
    sessionFactory = HibernateUtil.getSessionFactory();
    if (!checkexist(mapb)){
      Session session = sessionFactory.openSession();
      Transaction transaction =null;
      try {
        transaction = session.beginTransaction();
        PhongBan phongBan = (PhongBan) session.get(PhongBan.class,(Integer) mapb);
        session.delete(phongBan);
        transaction.commit();
      } catch (HibernateException e){
        if (transaction != null){
          transaction.rollback();
          e.printStackTrace();
        }
      }finally {
        session.close();
      }
    }
    else {
      System.out.println("Phòng ban không tồn tại...");
    }
  }

  public boolean checkexist(int mapb) {
    sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    Transaction transaction = null;
    try {
      transaction = session.beginTransaction();
      Query query = session.createQuery("from PhongBan");
      List<PhongBan> phongBans = (List<PhongBan>) query.list();
      for (PhongBan e : phongBans) {
        if (e.getMaPB() == mapb) {
          return false;
        }
      }
    } catch (HibernateException e) {
      if (transaction != null) {
        transaction.rollback();
        e.printStackTrace();
      }
    } finally {
      session.close();
    }
    return true;
  }

  public PhongBan find(int mapb){
    sessionFactory = HibernateUtil.getSessionFactory();
    PhongBan phongBan = null;
    if (!checkexist(mapb)){
      Session session = sessionFactory.openSession();
      Transaction  transaction = null;
      try {
        transaction = session.beginTransaction();
        Query pb = session.createQuery("from PhongBan");
        Query nv = session.createQuery("from NhanVien");
        phongBan = (PhongBan) session.load(PhongBan.class,new Integer(mapb));
        List<PhongBan> phongBans = (List<PhongBan>) pb.list();
        List<NhanVien> nhanViens = (List<NhanVien>) nv.list();
        for (PhongBan e :phongBans){
          if (e.getMaPB() == mapb){
            System.out.println("Tên phòng ban : " + e.getTenPB());
            System.out.println("Mã phòng ban  : " + e.getMaPB());
            System.out.println("Địa chỉ       : " + e.getDiachi());
            System.out.println("SDT           : " + e.getSdt());
            System.out.println("Danh sách nhân viên trong phòng ban: ");

          for (NhanVien f:nhanViens){
            if (f.getTenPB().equals(e.getTenPB())){
              System.out.println("Mã nv: "+f.getManv()+"        Họ tên: "+f.getHoTen());
            }
          }
        }
        }
      } catch (HibernateException e){
        if (transaction != null){
          transaction.rollback();
          e.printStackTrace();
        }
      }finally {
        session.close();
      }
    }
    else {
      System.out.println("Phòng ban không tồn tại ...");
    }
    return phongBan;
  }

  public List<PhongBan> list() {
    sessionFactory = HibernateUtil.getSessionFactory();
    Session session = sessionFactory.openSession();
    Transaction transaction = null;
    List<PhongBan> result = null;
      transaction = session.beginTransaction();
      Query query = session.createQuery("from PhongBan");
      result = (List<PhongBan>) query.list();
      transaction.commit();
//      System.out.println("PHÒNG BAN            ĐỊA CHỈ             SỐ ĐIỆN THOẠI"
//          + "       MÃ PHÒNG BAN ");
//      for (int i = 0; i < result.size(); i++) {
//        String a = " ";
//        String b = " ";
//        String c = " ";
//        for (int j = result.get(i).getTenPB().length(); j < 20; j++) {
//          a += " ";
//        }
//        for (int j = result.get(i).getDiachi().length(); j < 20; j++) {
//          b += " ";
//        }
//        for (int j = result.get(i).getSdt().length(); j < 20; j++) {
//          c += " ";
//        }
//        System.out.println(result.get(i).getTenPB() + a + result.get(i).getDiachi()
//            + b + result.get(i).getSdt()
//            + c + result.get(i).getMaPB());
//      }
////      }
    return result;
  }
  public void quanLy() {
    System.out.println("Các chức năng:");
    System.out.println("1: Thêm Phòng ban");
    System.out.println("2: Xóa Phòng ban");
    System.out.println("3: Sửa Phòng ban");
    System.out.println("4: Tìm Phòng ban");
    System.out.println("5: In ra danh sách Phòng ban");
    System.out.println("0: Thoát");
    boolean check = true;
    while (check){
      System.out.println();
      System.out.print("Nhập vào lựa chọn: ");
      int k = Nhap.nhapSoNguyen();
      switch (k){
        case 1:
          System.out.println("Nhập tên Phòng ban: ");
          String name = Nhap.nhapString();
          System.out.println("Nhập địa chỉ");
          String diachi = Nhap.nhapString();
          System.out.println("Nhập số điện thoại");
          String sdt = Nhap.nhapString();
          System.out.println("Nhập mã phòng ban");
          int maPB = Nhap.nhapSoNguyen();
          PhongBan phongban = new PhongBan(maPB,name,diachi,sdt);
          add(phongban);
          System.out.print("------------------");
          break;
        case 2:
          System.out.println("Nhập tên phòng ban cần xóa: ");
          maPB = Nhap.nhapSoNguyen();
          del(maPB);
          System.out.println("-------------------");
          break;
        case 3:
          System.out.println("Nhập mã pb: ");
          maPB = Nhap.nhapSoNguyen();
          System.out.println("Nhập tên mới: ");
          name = Nhap.nhapString();
          System.out.println("Nhập địa chỉ mới: ");
          diachi = Nhap.nhapString();
          System.out.println("Nhập sdt mới: ");
          sdt = Nhap.nhapString();
          phongban = new PhongBan(maPB,name,diachi,sdt);
          update(phongban);
          System.out.print("-------------------");
          break;
        case 4:
          System.out.println("Nhập mã phòng ban cần tìm: ");
          maPB = Nhap.nhapSoNguyen();
          find(maPB);
          System.out.print("-------------------");
          break;
        case 5:
          list();
          System.out.print("-------------------");
          break;
        case 0:
          check = false;
          break;
        default:
          System.out.println("Chưa có chức năng ...");
          break;
      }
    }
  }
}
