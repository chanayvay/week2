package com.chanayvay.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
  private static SessionFactory sessionFactory;
    static {
      try{
        sessionFactory = new AnnotationConfiguration().configure("spring.xml").buildSessionFactory();
      } catch (Throwable e){
        System.err.println("Intial SessionFactory Creation Failed");
        throw new ExceptionInInitializerError(e);
      }
    }

  public static SessionFactory getSessionFactory() {
    return sessionFactory;
  }
}
